## ORIENT DRIVER

The **[Orient Driver](https://bitbucket.org/fmreina/orient-driver/src/master/)** is a project to execute experiments to evaluate the impact over the time of the use of new **Integrity Constraints** (IC) added to the implementation of the **Graph Database** [OrientDB](https://orientdb.com/). It is part of a project that had the goal of adding support for the definition of integrity constraints on a graph database (DB).

For the development of our propose we chose the OrientDB database because its implementation already offered some support for the definition of simple contraints like value range, attribute type, unique value, among few others. With that our work extended this support to allow the declaration of new ICs.

The new ICs added to the OrientDB are:

- Conditional
- In/Out
- Required Edge
- Cardinality

This project is dependent of other two projects that are: *Constrains-Tests-Client* and *OrientDB-Server* on the versions *modified* and  *original*. 

The project **Constrains-Tests-Client** contains the tests with the different use cases for each of the proposed ICs. 

The project **OrientDB-Server** on the *modified* version contains the code of the OrientDB database with our modifications, extended to support the definition of the new ICs.

**Obs.: ** The folder *jar/* of this project already contains the compiled *.jar* of the dependent projects previouslly mentioned. The links for their source codes are listed below.

- [Constraint-Tests-Client](https://bitbucket.org/fmreina/constraint-tests-client/src/master/): Contains the tests used to evaluate the impact of the new constraints
- OrientDB server:
	- [Modified version](https://bitbucket.org/fmreina/orientdb/src/master/): Extended with the support for the new proposed constraints
	- [Original version](https://bitbucket.org/fmreina/orientdb/src/original/): Original code, without the support of the new constraints

---

## The Experiments:

The experiments were created to compare three variants of the OrientDB. The first one uses the *Original* OrientDB server, without modifications. The second uses the *Modified* version of the server, with the extended support for ICs. The last is the *Application* variant that uses the original server and executes the IC validation on the client-side before sending the command to the server.

For each measuring a new pair of JVMs (Java Virtual Machine) is created. One for the server and the other to the client.

One measuring consists of configuring the database (create the schema and populate it, when applicable) and of performing 100 transactions executed three times. Only the time of the  3rd execution is registered. These process is repeated 20 times in order to obtain 20 time registers.

---

## Reproducing The Experiment

To reproduce all the experiments execute **`$ ./run-all.sh`** on the terminal.

This command will execute the experiments as explained on the section *"The Experiments"*. 
It will make 3 executions of 100 transactions repeated 20 times for each of the variants of the DB (*Modified, Original, Application*) and for each of the constraints (*Conditional, In/Out, Required Edge, Cardinality*).

It is possible to edit some of the parameters in order to execute only part of the experiments.

The possible changes are on the **"test flavor/variant"**  (*Modified, Original, Application*), **constraint type** (*Conditional, In/Out, Required Edge, Cardinality*) and **number of repetitions** (20 by default).

To change the numebr of repetitions, use the flag **`REPETITIONS=<integer>`** before the command **`$ ./run-all.sh`** and the parameters **`<TEST_FLAVOR>`** and **`<CONSTRAINT_TYPE>`** after the command to set the wanted "test flavors"  and "constraint types".

The possible parameters are:

- REPETITIONS = < any integer >
- "Test flavors": **original, modified, application**
- Constraint types: **conditional, inout, requiredegde, cardinality**

**Example:** The command **`REPETITIONS=1 ./run-all.sh application inout`** will run the test for the constraint *In/Out* using the variant *Application* and will repeat only 1 time (3 executions of 100 transactions).

**Obs.:** The number of executions (3) and the number of transactions (100) are not parametrizable.

---

## Outputs:

#### **csv files:**
The execution will generate csv files that contains the time registered in each repetition. In the case of runnig the experiments with the default parameters, as explained in the section *"The Experiments"*, the output will be three csv files named:

 - **results-app.csv**: stores the values of the execution using the variant *Modified*
 - **results-original-app.csv**: stores the values of the execution using the variant *Original*
 - **results-application-app.csv**: stores the values of the execution using the variant *Application*

Each of these files contain the values that represent the time spent on the 3rd execution of every repetition for each constraint type.

In case of runnig the test for only one or two of the "test flavors", only the correspondent csv files will be generated as output. In a second execution of the *run-all.sh* file, the existing csv files are copied to a csv.bck file before ovewriting the previous values.

All these output files are stored in the folder *csv/* of this project.

#### **log files:**
The execution of the experiments will also generate a log file for each of the "test flavors" containing all the output showed in the console during the process. These files are stored in the folder *log/* of this project and are named as:

 - **test-app.log**: stores the log for the execution using the variant *Modified*
 - **test-original-app.log**: stores the log for the execution using the variant *Original*
 - **test-application-app.log**: stores the log for the execution using the variant *Application*

-----
