package br.ufsc.lapesd.driver;

import static java.lang.System.out;

public class Main {

	public static void main(String[] args) {
		out.println("\n>> Starting Driver...");
		
		new Driver(args);

		out.println("\n>> Ending Driver...");
	}

}
