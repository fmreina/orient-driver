package br.ufsc.lapesd.driver;

import org.apache.commons.io.input.TeeInputStream;

import java.io.*;
import java.util.Arrays;
import java.util.List;

import static java.lang.ProcessBuilder.Redirect.INHERIT;
import static java.lang.System.getProperty;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;

public class Driver {

  private String sep = getProperty("file.separator");
  private String serverJarName;
  private String testsJarName;
  private String testFalvor;

  public Driver(String[] args) {
    // for (String s : args) {
    // System.out.println("<<<< " + s + " >>>>");
    // }
    testFalvor = args[4];
    serverJarName = args[5];
    testsJarName = args[6];
    run(args[0], Integer.parseInt(args[1]), args[2], args[3]);
  }

  private static class ServerProcess {
    Process p;
    TeeInputStream stdOut, stdErr;

    ServerProcess(Process p) {
      this.p = p;
      this.stdOut = new TeeInputStream(p.getInputStream(), System.out);
      this.stdErr = new TeeInputStream(p.getErrorStream(), System.out);
    }
  }

  private ServerProcess createServer(String serverDir, String input) throws IOException {
    List<String> serverArgs = Arrays.asList(getProperty("java.home") + sep + "bin" + sep + "java", "-jar",
        new File(serverJarName).getAbsolutePath());
    ProcessBuilder builder = new ProcessBuilder(serverArgs).redirectOutput(ProcessBuilder.Redirect.PIPE)
        .redirectError(ProcessBuilder.Redirect.PIPE).directory(new File(serverDir));
    builder.environment().put("ORIENTDB_HOME", new File(serverDir).getAbsolutePath());
    builder.environment().remove("ORIENTDB_CONSOLE");
    Process p = builder.start();

    if (input != null) {
      try (PrintStream ps = new PrintStream(p.getOutputStream())) {
        ps.println(input);
      }
    }

    return new ServerProcess(p);
  }

  @SuppressWarnings("BooleanMethodIsAlwaysInverted")
  private boolean waitForAvailable(ServerProcess server) throws IOException {
    // StringBuilder sb = new StringBuilder();
    try (BufferedReader br = new BufferedReader(new InputStreamReader(server.stdErr))) {
      String line;
      while ((line = br.readLine()) != null) {
        // sb.append(line).append("\n");
//        System.err.println(line);
        if (line.contains("OrientDB Studio available"))
          return true;
      }
    }
    // System.err.println(sb.toString());
    return false;
  }

  private Process createChild(String mode, String test, String csvPath) throws IOException {
    List<String> childArgs = Arrays.asList(getProperty("java.home") + sep + "bin" + sep + "java", "-jar", testsJarName, mode, test,
        csvPath);
    return new ProcessBuilder(childArgs).redirectOutput(INHERIT).redirectError(INHERIT).start();
  }

  private void run(String testName, int repetitions, String csvPath, String serverDir) {
    try {
      System.out.println("\n>>> Starting the server for the first time to create the pwd");
      ServerProcess server = createServer(serverDir, "orientdb1234\norientdb1234");
      if (!waitForAvailable(server)) {
        System.err.println(">>> Error setting up server");
        System.exit(1);
      }

      server.p.destroy();
      if (!server.p.waitFor(30, SECONDS)) {
        System.out.println(">>> Server is refusing to die.");
        System.exit(1);
      }

      System.out.println("\n>>> Starting server for schema creation..");
      server = createServer(serverDir, null);
      if (!waitForAvailable(server)) {
        System.err.println(">>> Error setting up server");
        System.exit(1);
      }

      System.out.print("\n>>> Creating file.\n");
      createCSV(csvPath);

      System.out.println("\n>>> Creating schema..");
      Process child = createChild("schema_" + testFalvor, testName, csvPath);
      if (!child.waitFor(5, MINUTES)) {
        child.destroyForcibly().waitFor(1, MINUTES);
        System.out.println(">>> Child timed out");
      }

      server.p.destroy();
      if (!server.p.waitFor(30, SECONDS)) {
        System.out.println("\n>>> Server is refusing to die after schema configuration.");
        System.exit(1);
      }

      for (int i = 0; i < repetitions; ++i) {
        System.out.printf("\n>>> Runnig test %s i = %d\n", testName, i);

        System.out.println("\n>>> Starting server for tests");
        server = createServer(serverDir, null);
        if (!waitForAvailable(server)) {
          System.err.println(">>> Error setting up server after schema configuration");
          System.exit(1);
        }

        child = createChild("test_" + testFalvor, testName, csvPath);
        if (!child.waitFor(5, MINUTES)) {
          child.destroyForcibly().waitFor(1, MINUTES);
          System.out.println("\n>>> Child timed out after schema configuration");
        }

        server.p.destroy();
        if (!server.p.waitFor(30, SECONDS)) {
          System.out.println("\n>>> Server is refusing to die after test.");
          System.exit(1);
        }
      }

    } catch (IOException | InterruptedException e) {
      System.err.println("\n>>> Error trying to create child process!");
      e.printStackTrace();
    }
  }

  private void createCSV(String path) {
    File file = new File(path);
    if (file.exists()) {
      System.out.printf(">>> The file %s already exists.\n", path);
      return;
    }

    try (PrintWriter writer = new PrintWriter(file)) {
      System.out.printf(">>> File created: %s\n", path);
      writer.write("TestName,msecs\n");
    } catch (FileNotFoundException e) {
      System.out.println(e.getMessage());
    }
  }
}
