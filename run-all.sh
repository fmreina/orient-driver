#!/bin/bash
# Path Variables
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )" # absolute path of the sh file 

# .jar paths
DRIVER="$DIR/jar/orient-driver-1.0-SNAPSHOT.jar"
SERVER_JAR_BASE="$DIR/jar/orientdb-server-3.1.0"
TEST_JAR_BASE="$DIR/jar/constraint-tests-client-0.0.1"

# paths setted according to the TESTFLAVOR
PROJECT_VERSION=
SERVER_DIR=${SERVER_DIR:-"${DIR}/serverdir"}
mkdir -p "$SERVER_DIR/config"

# test variables
FLAVORS=${1:-"original modified application"} 
TESTS=${2:-"conditional inout requirededge cardinality bidirectionaledge edgecondition"} 
REPETITIONS=${REPETITIONS:-20} # 

echo ">>Building orient-driver"
cd "$DIR"
mvn package

for TESTFLAVOR in $(echo $FLAVORS); do
  echo "TESTFLAVOR=$TESTFLAVOR"
  if [ "$TESTFLAVOR" =  modified ] ; then
    PROJECT_VERSION="0.0.1-CUSTOM"
    SERVER_JAR="${SERVER_JAR_BASE}-CUSTOM.jar"
    TEST_JAR="${TEST_JAR_BASE}-CUSTOM.jar"
    CSV="$DIR/csv/results-app.csv"
    LOG="$DIR/log/test-app.log"
    echo "RUNNING ON MODIFIED"
  elif [ "$TESTFLAVOR" =  original ] ; then
    PROJECT_VERSION="0.0.1-SNAPSHOT"
    SERVER_JAR="${SERVER_JAR_BASE}-SNAPSHOT.jar"
    TEST_JAR="${TEST_JAR_BASE}-SNAPSHOT.jar"
    CSV="$DIR/csv/results-original-app.csv"
    LOG="$DIR/log/test-original-app.log"
    echo "RUNNING ON ORIGINAL"
  elif [ "$TESTFLAVOR" =  application ] ; then
    PROJECT_VERSION="0.0.1-SNAPSHOT"
    SERVER_JAR="${SERVER_JAR_BASE}-SNAPSHOT.jar"
    TEST_JAR="${TEST_JAR_BASE}-SNAPSHOT.jar"
    CSV="$DIR/csv/results-application-app.csv"
    LOG="$DIR/log/test-application-app.log"
    echo "RUNNING ON APPLICATION"
  else
    echo "Failed to define server jar!"
    exit 1
  fi
  
  cd "$DIR"
  
  # backup results and log for current $TESTFLAVOR

  if [ -f "$CSV" ] ; then
    echo "Results file $CSV moved to $CSV.bck"
    mv "$CSV" "$CSV.bck" 
  fi
  if [ -f "$LOG" ] ; then
    echo "Log file $LOG moved to $LOG.bck"
    mv "$LOG" "$LOG.bck" &>/dev/null || true 
  fi
  # Ensure log dir exists. Not needed for csv/ since csvs are in git
  mkdir -p $(dirname "$LOG")
  
  # tests execution
  START=$(date +%s)
  for t in $(echo $TESTS); do
          # remove whole database. Will be initialized by the Java code
    rm -f "$SERVER_DIR/config/orientdb-server-config.xml"
    rm -fr "$SERVER_DIR/databases"
          # Any component in the pipe triggers a failure, not only tee
          set -o pipefail
    if ! (java -jar $DRIVER $t $REPETITIONS $CSV "$SERVER_DIR" $TESTFLAVOR $SERVER_JAR $TEST_JAR 2>&1 | tee -a "log/test.log") ; then
            echo "Driver failed with exit code $? aborting all experiments"
      exit 1 
    fi  
  done
done

END=$(date +%s)
DIFF=$(( $END - $START ))
echo ">> Total time spent: $DIFF seconds"

